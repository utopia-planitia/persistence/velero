{{ if .Values.enabled }}
apiVersion: velero.io/v1
kind: Schedule
metadata:
  name: daily-full
  namespace: velero
spec:
  schedule: {{ .Values.schedule }}
  template:
    includedNamespaces:
      - "*"
    ttl: 720h0m0s
{{ end }}
