#!/bin/bash
set -exuo pipefail

CHART_VERSION=$(yq -r '.releases[] | select( .chart == "vmware-tanzu/velero" ) | .version' velero/helmfile.yaml | sort | uniq)

echo "CHART_VERSION: ${CHART_VERSION}"

rm -f velero-crds/chart/templates/*

curl -L -s --fail https://github.com/vmware-tanzu/helm-charts/archive/velero-${CHART_VERSION}.tar.gz \
    | tar -C velero-crds/chart/templates -xzf - helm-charts-velero-${CHART_VERSION}/charts/velero/crds/ --strip-components=4

chart-prettier --stdin=false ./velero-crds/chart/templates
