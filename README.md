# Velero

Deployment of Velero to kubernetes.

https://velero.io/

## Usage

https://velero.io/docs/main/api-types/

``` yaml
apiVersion: velero.io/v1
kind: Schedule
metadata:
  name: daily-full
  namespace: velero
spec:
  schedule: 0 5 * * *
  template:
    defaultVolumesToRestic: true
    includedNamespaces:
    - '*'
    ttl: 720h0m0s
```

## Logs

Velero does not show logs before a task is done.

Show all logs of a backup
``` bash
velero backup logs <backup-name>
```

Show errors of a backup
``` bash
velero backup logs <backup-name> | grep error
```

Show warnings of a restore
``` bash
velero restore logs <restore-name> | grep warning
```

## install client

``` bash
curl -L --fail https://github.com/vmware-tanzu/velero/releases/download/v1.6.0/velero-v1.6.0-linux-amd64.tar.gz \
  | tar -xzO velero-v1.6.0-linux-amd64/velero \
  > /usr/bin/velero
chmod +x /usr/bin/velero
grep "velero completion bash" ~/.bashrc > /dev/null || ( echo "source <(velero completion bash)" >> ~/.bashrc )
```
