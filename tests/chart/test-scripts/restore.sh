#!/usr/bin/bash
set -euxo pipefail

VELERO_EXEC="kubectl exec deployment.apps/velero --namespace=velero -- /velero"

reset() {
  kubectl delete namespace velero-backup-test --ignore-not-found=true
  if ${VELERO_EXEC:?} backup get backup-test; then
    ${VELERO_EXEC:?} backup delete backup-test --confirm
  fi
  if ${VELERO_EXEC:?} restore get backup-test; then
    ${VELERO_EXEC:?} restore delete backup-test --confirm
  fi
}
reset

# setup
kubectl create namespace velero-backup-test
kubectl apply -f example-app.yml
sleep 2
kubectl -n velero-backup-test wait --for=condition=available --timeout=600s deployment/backup-data

kubectl -n velero-backup-test wait --for=condition=available --timeout=600s deployment/backup-data
kubectl -n velero-backup-test exec deployment/backup-data -- sh -c 'echo "Hello, world." > /backup-data/test.txt'

# verify
kubectl -n velero-backup-test exec deployment/backup-data -- sh -c 'cat /backup-data/test.txt | grep "Hello, world."'

# backup
${VELERO_EXEC:?} backup delete backup-test --confirm # cleanup previous run
if ! timeout 300 ${VELERO_EXEC:?} backup create backup-test --include-namespaces velero-backup-test --default-volumes-to-fs-backup=true --wait; then
  ${VELERO_EXEC:?} backup describe backup-test
  # Our integration cluster uses a Let's Encrypt Staging certificate so the certificate authority is unknown to Velero.
  ${VELERO_EXEC:?} backup logs backup-test --insecure-skip-tls-verify
  exit 1
fi

# check
${VELERO_EXEC:?} backup get backup-test
${VELERO_EXEC:?} backup describe backup-test
# Our integration cluster uses a Let's Encrypt Staging certificate so the certificate authority is unknown to Velero.
${VELERO_EXEC:?} backup logs backup-test --insecure-skip-tls-verify

# destroy
timeout 300 kubectl delete namespace velero-backup-test

# restore
timeout 300 ${VELERO_EXEC:?} restore create restore-test --from-backup backup-test --wait
sleep 2
kubectl -n velero-backup-test wait --for=condition=available --timeout=600s deployment/backup-data

# verify
kubectl -n velero-backup-test exec deployment/backup-data -- sh -c 'cat /backup-data/test.txt | grep "Hello, world."'

reset
